import tkinter as tk
from tkinter import filedialog
import tkinter.font as tkFont
from PIL import Image,ImageTk
import os
import shutil


class ImageClassifyer(tk.Frame):


    def __init__(self, parent, *args, **kwargs):

        tk.Frame.__init__(self, parent, *args, **kwargs)


        self.folder_selected = ""
        self.destination_folder = "/Users/ian/Downloads/cifar-10-100/horse/"
        self.root = parent
        self.root.wm_title("Classify Image")
        self.src = ""
        fontStyle = tkFont.Font(family="Lucida Grande", size=20)
        self.list_images = []
        self.destination = ""

        self.frame1 = tk.Frame(self.root, width=700, height=400, bd=2)
        self.frame1.grid(row=1, columnspan=5)
        self.final = tk.Label(self.frame1, text='please choose folder', font=fontStyle)
        self.final.place(x=30, y=10)

        # self.frame2 = tk.Frame(self.root, width=500, height=400, bd=1)
        # self.frame2.grid(row=1, column=1)


        # self.cv2 = tk.Canvas(self.frame2, height=390, width=490, bd=2, relief=tk.SUNKEN)
        # self.cv2.grid(row=1, column=0)

        happiness_Button = tk.Button(self.root, text='happiness(q)', height=2, width=10, command=self.classify_obj)
        happiness_Button.grid(row=0, column=2)

        sadness_Button = tk.Button(self.root, text='sadness(w)', height=2, width=10, command=self.classify_obj)
        sadness_Button.grid(row=0, column=3)

        fear_Button = tk.Button(self.root, text='fear(e)', height=2, width=10, command=self.classify_obj)
        fear_Button.grid(row=0, column=4, padx=3, pady=3)

        surprise_Button = tk.Button(self.root, text='surprise(r)', height=2, width=10, command=self.classify_obj)
        surprise_Button.grid(row=0, column=5, padx=3, pady=3)

        anger_Button = tk.Button(self.root, text='anger(t)', height=2, width=10, command=self.classify_obj)
        anger_Button.grid(row=0, column=6, padx=3, pady=3)

        # broButton = tk.Button(self.root, text='Next', height=2, width=8, command = self.next_image)
        # broButton.grid(row=0, column=6, padx=3, pady=3)
        # self.root.bind("<Left>", lambda e: self.previous_image())

        file = tk.Button(self.root, text='Browse', height=2, width=10, command=self.choose_folder)
        file.grid(row=0, column=1, padx=2, pady=2)
        # choose = tk.Label(self.root, text="Choose file", height=2, width=10)
        # # choose.grid(row=1, column=2, padx=2, pady=2)
        # broButton.bind("<Right>", self.next_image)

        self.counter = 0

        self.max_count = 0
        # self.next_image()
        # self.root.bind("<Right>", lambda e: self.next_image())
        self.root.bind("<q>", lambda e: self.move_image("happiness"))
        self.root.bind("<w>", lambda e: self.move_image("sadness"))
        self.root.bind("<e>", lambda e: self.move_image("fear"))
        self.root.bind("<r>", lambda e: self.move_image("surprise"))
        self.root.bind("<t>", lambda e: self.move_image("anger"))


        # print(self.folder_selected)


    def choose_folder(self):
        # ifile = filedialog.askdirectory(parent=self,mode='rb',title='Choose a file')

        self.final.place_forget()
        folder_selected = filedialog.askdirectory()
        self.folder_selected = folder_selected + "/"
        self.first_image()

    def classify_obj(self, img, emo):
        print(emo)
        # src = "/Users/ian/Downloads/cifar-10-100/horse/" + img
        self.src = self.folder_selected + img
        self.destination = self.destination_folder + emo + "/"
        shutil.move(self.src, self.destination)

    # def previous_image(self):

    def first_image(self):
        self.src = self.folder_selected
        self.destination = ""
        self.list_images = []
        for d in os.listdir(self.src):
            if d.endswith("png") or d.endswith("jpg"):
                self.list_images.append(d)

        self.max_count = len(self.list_images)-1

        # if self.counter > self.max_count:
        #     print("No more images")
        # else:
        #     im = Image.open("{}{}".format("/Users/ian/Downloads/cifar-10-100/horse/", self.list_images[self.counter]))
        #     if (490-im.size[0])<(390-im.size[1]):
        #         width = 490
        #         height = width*im.size[1]/im.size[0]
        #         self.next_step(height, width)
        #     else:
        #         height = 390
        #         width = height*im.size[0]/im.size[1]
        #         self.next_step(height, width)
        self.counter = 0
        # self.frame1 = tk.Frame(self.root, width=500, height=400, bd=2)
        # self.frame1.grid(row=1, columnspan=5)
        self.cv1 = tk.Canvas(self.frame1, height=400, width=700, background="white", bd=1, relief=tk.RAISED)
        self.cv1.grid(row=1, columnspan=5)
        im = Image.open("{}{}".format(self.folder_selected, self.list_images[0]))
        if (490 - im.size[0]) < (390 - im.size[1]):
            width = 490
            height = width * im.size[1] / im.size[0]
            self.next_step(height, width)
        else:
            height = 390
            width = height * im.size[0] / im.size[1]
            self.next_step(height, width)

    def next_image(self):

        fontStyle = tkFont.Font(family="Lucida Grande", size=16)

        if self.counter > self.max_count:
            self.frame1.grid_forget()
            self.frame1 = tk.Frame(self.root, width=400, height=700, bd=2)
            self.frame1.grid(row=1, columnspan=5)
            self.final = tk.Label(self.frame1, text='This folder is classified, please choose another folder',
                                  font=fontStyle)
            self.final.place(x=30, y=10)

        else:
            # else:
            #     im = Image.open("{}{}".format("/Users/ian/Downloads/cifar-10-100/horse/", self.list_images[self.counter]))
            #     if (490-im.size[0])<(390-im.size[1]):
            #         width = 490
            #         height = width*im.size[1]/im.size[0]
            #         self.next_step(height, width)
            #     else:
            #         height = 390
            #         width = height*im.size[0]/im.size[1]
            #         self.next_step(height, width)
            # print(self.folder_selected)
            im = Image.open("{}{}".format(self.folder_selected, self.list_images[self.counter]))
            if (490 - im.size[0]) < (390 - im.size[1]):
                width = 490
                height = width * im.size[1] / im.size[0]
                self.next_step(height, width)
            else:
                height = 390
                width = height * im.size[0] / im.size[1]
                self.next_step(height, width)

    def move_image(self, emo):
        fontStyle = tkFont.Font(family="Lucida Grande", size=20)

        if self.counter > self.max_count:
            self.cv1 = tk.Canvas(self.frame1, height=700, width=490, background="white", bd=1, relief=tk.RAISED)
            tk.Label(self.frame1, text='This folder is classified, please choose another folder', font=fontStyle).place(x=50, y=70)
        else:
            im = Image.open("{}{}".format(self.folder_selected, self.list_images[self.counter]))
            if (490-im.size[0])<(390-im.size[1]):
                width = 490
                height = width*im.size[1]/im.size[0]
                self.next_step(height, width)

            else:
                height = 390
                width = height*im.size[0]/im.size[1]
                self.next_step(height, width)
            self.classify_obj(self.list_images[self.counter], emo)
            self.counter += 1
            self.next_image()

    def next_step(self, height, width):
        self.im = Image.open("{}{}".format(self.folder_selected, self.list_images[self.counter]))
        self.im.thumbnail((width, height), Image.ANTIALIAS)
        self.root.photo = ImageTk.PhotoImage(self.im)
        self.photo = ImageTk.PhotoImage(self.im)

        if self.counter == 0:
            self.cv1.create_image(0, 0, anchor = 'nw', image = self.photo)

        else:
            self.im.thumbnail((width, height), Image.ANTIALIAS)
            self.cv1.delete("all")
            self.cv1.create_image(0, 0, anchor = 'nw', image = self.photo)
        # self.counter += 1


if __name__ == "__main__":
    root = tk.Tk()
    MyApp = ImageClassifyer(root)
    tk.mainloop()